# -*- coding: utf-8 -*-
import scrapy
from scrapy.selector import Selector
from tutorial.items import DiputadoItem

class DiputadosSpider( scrapy.Spider ):
	"""docstring for DiputadosSpider """

	name = "diputados"
	start_urls = [
		"http://sitl.diputados.gob.mx/LXII_leg/listado_diputados_gpnp.php?tipot="
	]

	def parse( self, response ):
		sel = Selector( response )
		#dips = sel.xpath( '//td[@class="textoNegro"]' )
		dips = sel.xpath( '//tr' )
		#diputados = []

		for dip in dips:
			item = DiputadoItem()
			
			#item[ 'diputado_id' ] = dip.xpath( 'a/@href' ).extract()
			#item[ 'nombre' ] = dip.xpath( 'a/text()' ).extract()
			#item[ 'entidad' ] = dip.xpath( 'text()' ).extract()
			#item[ 'distrito' ] = dip.xpath( 'text()' ).extract()
			item[ 'diputado_id' ] = dip.xpath( 'td[@class="textoNegro"]/a/@href' ).re('\d+')
			item[ 'nombre' ] = dip.xpath( 'td[@class="textoNegro"]/a/text()' ).extract()
			item[ 'entidad' ] = dip.xpath( 'td[2][@class="textoNegro"]/text()' ).extract()
			item[ 'distrito' ] = dip.xpath( 'td[3][@class="textoNegro"]/text()' ).extract()
			if item['diputado_id']:
				#diputados.append( item )
				yield item
			
