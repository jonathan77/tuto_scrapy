import scrapy
from scrapy.selector import Selector
from tutorial.items import DmozItem

class DmozSpider( scrapy.Spider ):
	"""docstring for DmozSpider"""

	name = "dmoz"
	start_urls = [
		"http://www.dmoz.org/Computers/Programming/Languages/Python/Books/",
		"http://www.dmoz.org/Computers/Programming/Languages/Python/Resources/"
	]

	def parse( self, response ):
		#filename = response.url.split( "/" )[ -2 ]
		#with open( filename, "wb" ) as f:
			#f.write( response.body )

		sel = Selector(response)
		sites = sel.xpath( '//ul[@class="directory-url"]/li' )
		items = []

		for site in sites:
			item = DmozItem()
			item[ 'title' ] = site.xpath( 'a/text()' ).extract()
			item[ 'link' ] = site.xpath( 'a/@href' ).extract()
			item[ 'desc' ] = site.xpath( 'text()' ).re('-\s[^\n]*\\r')
			yield item

		"""
		for sel in response.xpath('//ul/li'):
            item = DmozItem()
            item['title'] = sel.xpath('a/text()').extract()
            item['link'] = sel.xpath('a/@href').extract()
            item['desc'] = sel.xpath('text()').extract()
            yield item
		"""
